Devnotebook4
============

Générateur de blog à partir d'articles en fichiers `.md`.

Le générateur recherche les fichiers `.md` récursivement dans un répertoire racine, les transforme en HTML et les rend.

## Parsage des fichiers

Les informations suivantes sont extraites d'un fichier article :

- le titre : 1ère ligne du fichier (doit commencer par `#` ou être soulignée par `==`)
- le slug : généré à partir du titre 
- les métadonnées : type, version, sources, dates de création/maj, tags (cf. ci-dessous)
- le contenu : tout ce qui est présent dans le fichier après le titre et les métadonnées

### Métadonnées

Les métadonnées peuvent être ajoutées au fichier sous la forme d'une liste de liens références markdown :

Ex:

```markdown
[:type]: # "Astuce"
[:version]: # "Debian 8, Apache 2.4"
[:sources]: # "[www.lafermeduweb.net](http://www.lafermeduweb.net/billet/tutorial-creer-un-serveur-web-complet-sous-debian-1-apache-160.html#InstApache2) | [source 2](fr.wikipedia.org)"
[:created_at]: # "2017/07/20"
[:modified_at]: # "2020/11/02"
[:tags]: # "linux-unix systemes-reseaux apache debian"
```

**Explications :**

Le **type** d'article (`Astuce`, `Marque-page` ou `Erreur`) permet d'afficher un picto différent devant le titre des articles
(respectivement une ampoule, un marque-page ou une croix rouge).  
La **version** indique à quelle version d'une techno, d'un environnement l'article se rapporte.  
Les **tags** sont les mots-clés utilisés dans le nuage de tags, pour regrouper les articles. 
Le premier de la liste est considéré comme le tag principal. Il est ajouté à l'URL de l'article.

### Images

Les images utilisées dans les articles doivent porter le même nom de fichier que l'article (excepté l'extension), 
suffixé d'un numéro à deux chiffres.

Ex: 

L'article **Un formateur et validateur JSON** est constitué d'un fichier json et de deux images :

- un-formateur-et-validateur-json.md
- un-formateur-et-validateur-json-01.png
- un-formateur-et-validateur-json-02.png

Ces images seront automatiquement copiées dans un répertoire public du serveur web pour qu'il puisse les servir.

### URL internes

Les URLs internes sont remplacées automatiquement dans le contenu des articles.

Toutes celles vers des images locales doivent être relatives à l'article. Cela permet de les afficher correctement lorsqu'on
édite l'article avec un éditeur en local.

Ex dans le fichier `mon-article.md` :
```markdown
![mon image](./mon-article-01.png)
```

Celles pointant vers d'autres articles doivent être relatifs au répertoire racine des articles. 

Ex dans le fichier `tag-1/mon-article.md` :
```markdown
[Mon autre article](./tag-1/mon-autre-article)
[Un article avec un autre tag](./tag-2/mon-autre-article)
```

### Fichiers téléchargeables

Les fichiers téléchargeables depuis les articles ne sont pas gérés par l'application.
Ils doivent être hébergés ailleurs et les articles utilisent des URL absolus pour pointer dessus.

Pour faciliter l'hébergement, des fichiers sont versionnés sur le même dépôt Git que les articles, dans le répertoire
`_files/`. Puisqu'il est public, on peut utiliser l'url de téléchargement fournie par Gitlab directement
dans les articles.

## Configuration

Les chemins vers les articles et leurs assets sont configurables en `.env` :

```dotenv
# Chemin absolu vers le répertoire racine contenant les articles `.md`
ARTICLE_DIR_NAME_PATH=/srv/app/blog/articles
# Chemin absolu vers le répertoire racine qui contiendra les assets associés aux articles
# Les fichiers dans ce répertoire doivent être servables par le serveur web
ARTICLE_ASSET_DIR_NAME_PATH=/srv/app/public/articles
```

## Cache

Plusieurs éléments sont mis en cache

- chaque article, une fois construit à partir d'un fichier `.md` parsé
- l'ensemble des articles, une fois construits à partir des fichiers `.md` parsés
- le nuage de tags, une fois construit 

Chacun de ces éléments est caché dans un pool spécifique (cf. `blog/config/packages/cache.yaml`) :

- `articles_cache_pool`
- `tag_cloud_cache_pool`

Ils peuvent être vidés individuellement. Ex: 

```bash
bin/console cache:pool:clear articles_cache_pool
```

### Clés de cache 

La clé de cache d'un article est construite à partir du hash MD5 de son contenu. Si le
contenu est modifié, le hash change et la nouvelle version est donc prise en compte automatiquement.

La clé de cache de l'ensemble des articles contient le nombre d'articles. Si un article est ajouté,
le cache est régénéré.

## Environnement de dev

Pour utiliser l’application en local :

- Clonez le dépôt
- Lancez les commandes suivantes :

```bash
cd blog
make build-all
make up
```

L’application est accessible via <https://127.0.0.1:8000>

Pour accéder au bash du conteneur docker, lancez la commande :

```bash
make exec-bash
```

### Assets

Les assets sont générés par **Encore** au lancement des conteneurs.  
Normalement les modifications sont mises à jour en live dans le répertoire `public/`.
Il suffit de recharger la page pour les prendre en compte.

## RSS

Un flux RSS est disponible via la page `/feed`.
