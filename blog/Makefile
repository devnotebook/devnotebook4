USER_ID=$(shell id -u)
GROUP_ID=$(shell id -g)
COMPOSE_FILE=./docker/compose.yml

.DEFAULT_GOAL := help
help:
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[36m\033[0m\n"} /^[0-9a-zA-Z_-]+:.*?##/ { printf "  \033[36m%-30s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' $(MAKEFILE_LIST)

docker-compose-user-exec = docker compose -f ${COMPOSE_FILE} exec -T --user ${USER_ID}:${GROUP_ID} ${1}
docker-compose-root-exec = docker compose -f ${COMPOSE_FILE} exec -T ${1}


# -------------
# Lancement des containers
# -------------

up: ## Lancement de l'environnement de dev
	docker compose -f ${COMPOSE_FILE} up

down: ## Arrêt de l'environnement de dev
	docker compose -f ${COMPOSE_FILE} down


# -------------
# Construction des images docker
# -------------

image-build-base: ## Reconstruction de l’image de base
	./docker/build.sh base --no-push

image-build-dev: ## Reconstruction de l’image de dev
	./docker/build.sh dev --no-push

image-build-all: image-build-base image-build-dev ## Reconstruction de l’image frontend pour le dev


## -------------
## Outils
## -------------

exec-cmd: ## Éxécution d’une commande bash dans le conteneur (make exec-cmd CMD="ls .")
	$(call docker-compose-user-exec, php $(CMD))

exec-bash: ## Lancement du terminal dans le conteneur principal
	docker compose -f ${COMPOSE_FILE} exec php bash

exec-encore: ## Lancement du terminal dans le conteneur encore
	$(call docker-compose-user-exec, encore $(CMD))

cache-clear: ## Vidage du cache
	$(call docker-compose-user-exec, php php bin/console cache:clear)

phpcs: ## Exécuter PHP Check Style
	docker run -i --rm -v $(shell pwd):/srv/sources:ro registry.gitlab.com/v.nivuahc/docker-tools/phpcs:latest

phpstan: ## Exécuter PHP Stan
	docker compose -f ./docker/compose-phpstan.yml exec --user ${USER_ID}:${GROUP_ID} php \
	php -d memory_limit=-1 vendor/bin/phpstan.phar analyse -l 8 src

package: ## Builder et packager l’application
	mkdir -p ci_build
	sudo rm -Rf vendor
	mv .env.local save.env.local
	make exec-cmd CMD="composer install --no-dev --optimize-autoloader"
	docker run -i --rm -v $(shell pwd):/srv/sources:rw registry.gitlab.com/v.nivuahc/docker-tools/package:latest
	sudo rm -Rf vendor
	mv save.env.local .env.local
	make exec-cmd CMD="composer install"
