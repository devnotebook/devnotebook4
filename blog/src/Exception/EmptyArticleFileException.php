<?php

namespace App\Exception;

class EmptyArticleFileException extends \InvalidArgumentException
{
}
