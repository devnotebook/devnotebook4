<?php

namespace App\Exception;

class ArticleWithNoTagException extends \InvalidArgumentException
{
}
