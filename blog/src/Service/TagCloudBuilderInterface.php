<?php

namespace App\Service;

use App\Entity\CloudTag;

interface TagCloudBuilderInterface
{
    /**
     * Construit le nuage de tags et en retourne les éléments.
     *
     * @return CloudTag[]|array
     */
    public function build(): array;
}
