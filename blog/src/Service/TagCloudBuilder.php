<?php

namespace App\Service;

use App\Entity\Article;
use App\Entity\CloudTag;
use Symfony\Component\Cache\Adapter\AdapterInterface as CacheManager;

class TagCloudBuilder implements TagCloudBuilderInterface
{
    private ArticleRetrieverInterface $articleRetriever;
    private CacheManager $cacheManager;

    public function __construct(
        ArticleRetrieverInterface $articleRetriever,
        CacheManager $cacheManager
    ) {
        $this->articleRetriever = $articleRetriever;
        $this->cacheManager = $cacheManager;
    }

    /**
     * @inheritDoc
     */
    public function build(): array
    {
        $item = $this->cacheManager->getItem('tags|cloud');
        if (!$item->isHit()) {
            $allArticles = $this->articleRetriever->retrieveAllArticles();

            $tagWeights = $this->getTagWeights($allArticles);
            $item->set($this->buildTagCloud($tagWeights));

            $this->cacheManager->save($item);
        }

        return $item->get();
    }

    /**
     * Retourne le nuage de tags dans un ordre aléatoire.
     *
     * Les poids des tags de la liste sont uniformisés pour avoir des valeurs comprises entre 0 et 5.
     *
     * @param CloudTag[]|array $tagList
     *
     * @return CloudTag[]|array
     */
    private function buildTagCloud(array $tagList): array
    {
        $tagCloud = [];
        $nbOfTags = \count($tagList);
        foreach ($tagList as $tag) {
            $weight = $tag->getWeight();

            switch (true) {
                case $weight < 0.1 * $nbOfTags:
                    $tagCloud[] = $tag->setWeight(0);
                    break;

                case $weight >= 0.1 * $nbOfTags && $weight < 0.3 * $nbOfTags:
                    $tagCloud[] = $tag->setWeight(1);
                    break;

                case $weight >= 0.3 * $nbOfTags && $weight < 0.5 * $nbOfTags:
                    $tagCloud[] = $tag->setWeight(2);
                    break;

                case $weight >= 0.5 * $nbOfTags && $weight < 0.7 * $nbOfTags:
                    $tagCloud[] = $tag->setWeight(3);
                    break;

                case $weight >= 0.7 * $nbOfTags && $weight < 0.9 * $nbOfTags:
                    $tagCloud[] = $tag->setWeight(4);
                    break;

                case $weight > 0.9 * $nbOfTags:
                    $tagCloud[] = $tag->setWeight(5);
                    break;
            }
        }

        shuffle($tagCloud);

        return $tagCloud;
    }

    /**
     * Retourne la liste des tags avec un poids relatif au nombre de tags.
     *
     * @param Article[]|array $articles
     *
     * @return CloudTag[]|array
     */
    private function getTagWeights(array $articles): array
    {
        $tagList = [];
        foreach ($articles as $article) {
            foreach ($article->getTags() as $tagIndex => $tagName) {
                if (!\array_key_exists($tagName, $tagList)) {
                    $tagList[$tagName] = new CloudTag($tagName);
                }

                $tagList[$tagName]->incNbOfArticles();
                $newWeight = $tagList[$tagName]->getWeight() + $this->getTagWeight($tagIndex);
                $tagList[$tagName]->setWeight($newWeight);
            }
        }

        $tagList = array_values($tagList);

        usort(
            $tagList,
            static function ($tag1, $tag2) {
                return $tag2->getWeight() <=> $tag1->getWeight();
            }
        );

        return $tagList;
    }

    /**
     * Retourne le poids du tag selon sa place dans la liste des tags de l'article.
     *
     * @param int $index
     *
     * @return float
     */
    private function getTagWeight(int $index): float
    {
        if ($index === 0) {
            return 1.5;
        }

        if ($index === 1) {
            return 0.15;
        }

        return 0.1;
    }
}
