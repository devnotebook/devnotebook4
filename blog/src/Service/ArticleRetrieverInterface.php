<?php

namespace App\Service;

use App\Entity\Article;
use App\Exception\ArticleNotFoundException;

interface ArticleRetrieverInterface
{
    /**
     * Retourne l'article ayant pour premier tag et pour slug ceux en arguments.
     *
     * @param string $mainTag
     * @param string $slug
     *
     * @return Article
     *
     * @throws ArticleNotFoundException
     */
    public function retrieveArticle(string $mainTag, string $slug): Article;

    /**
     * Retourne tous les articles triés par date de publication antéchronologique.
     *
     * @return Article[]|array
     */
    public function retrieveAllArticles(): array;

    /**
     * Retourne tous les articles ayant le tag en argument, triés par date de publication antéchronologique.
     *
     * @param string $tag
     *
     * @return Article[]|array
     */
    public function retrieveArticlesByTag(string $tag): array;

    /**
     * Retourne tous les articles contenant le terme en argument.
     *
     * @param string $term
     *
     * @return Article[]|array
     */
    public function retrieveArticlesByTerm(string $term): array;
}
