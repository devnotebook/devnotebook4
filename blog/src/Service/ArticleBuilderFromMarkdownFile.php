<?php

namespace App\Service;

use App\Entity\Article;
use App\Entity\Link;
use App\Exception\ArticleWithNoTagException;
use App\Exception\EmptyArticleFileException;
use App\Exception\NoTitleFoundException;
use Symfony\Component\Finder\SplFileInfo;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\String\Slugger\SluggerInterface;

class ArticleBuilderFromMarkdownFile implements ArticleBuilderInterface
{
    private RequestStack $requestStack;
    private SluggerInterface $slugger;
    private string $articleAssetPath;

    public function __construct(
        SluggerInterface $slugger,
        RequestStack $requestStack,
        string $articleAssetPath
    ) {
        $this->slugger = $slugger;
        $this->requestStack = $requestStack;
        $this->articleAssetPath = $articleAssetPath;
    }

    /**
     * @inheritDoc
     */
    public function buildArticle(SplFileInfo $file): Article
    {
        $articleFileParts = $this->explodeArticleFile($file);

        return $this->getArticleInstance(
            $articleFileParts['title'],
            $articleFileParts['slug'],
            $articleFileParts['metadata'],
            $articleFileParts['content'],
        );
    }

    /**
     * Extrait les différentes données de l'article contenues dans le fichier.
     *
     * @param SplFileInfo $file
     *
     * @return mixed[]|array
     *
     * @throws EmptyArticleFileException
     * @throws NoTitleFoundException
     * @throws ArticleWithNoTagException
     */
    private function explodeArticleFile(SplFileInfo $file): array
    {
        $fileContent = $file->getContents();

        $fileLines = explode("\n", $fileContent);
        if (\count($fileLines) < 3) {
            throw new EmptyArticleFileException(
                sprintf(
                    'The file "%s" does not contain a valid article.',
                    $file->getPathname(),
                )
            );
        }

        try {
            $title = $this->parseTitle($fileLines);
        } catch (NoTitleFoundException $e) {
            throw new NoTitleFoundException(
                sprintf(
                    'The file "%s" does not contain a valid title.',
                    $file->getPathname(),
                ),
                0,
                $e
            );
        }

        $slug = strtolower($this->slugger->slug($title));
        $metadata = $this->parseMetadata($fileLines);
        $mainTag = $metadata['tags'][0] ?? null;
        $content = $this->modifyInternalUrl(
            implode("\n", $fileLines),
            $mainTag
        );

        if (\count($metadata['tags']) === 0) {
            throw new ArticleWithNoTagException(
                sprintf(
                    'The article file "%s" does not have any tag.',
                    $file->getPathname(),
                )
            );
        }

        return [
            'title' => $title,
            'slug' => $slug,
            'metadata' => $metadata,
            'content' => $content,
        ];
    }

    /**
     * Retourne une instance d'article avec les données en argument.
     *
     * @param string $title
     * @param string $slug
     * @param mixed[]|array $metadata
     * @param string $htmlContent
     *
     * @return Article
     */
    private function getArticleInstance(
        string $title,
        string $slug,
        array $metadata,
        string $htmlContent
    ): Article {
        return (new Article())
            ->setTitle($title)
            ->setType($metadata['type'])
            ->setSlug($slug)
            ->setContent($htmlContent)
            ->setSources($metadata['sources'])
            ->setVersion($metadata['version'])
            ->setTags($metadata['tags'])
            ->setCreatedAt($metadata['created_at'])
            ->setModifiedAt($metadata['modified_at'] ?? $metadata['created_at']);
    }

    /**
     * Parse le titre dans les lignes du fichier.
     *
     * Retourne ce titre et le retire des lignes.
     *
     * @param string[]|array $fileLines Lignes du fichier (au moins 3)
     *
     * @return string
     *
     * @throws NoTitleFoundException
     */
    private function parseTitle(array &$fileLines): string
    {
        $hasTitle = str_starts_with($fileLines[0], '#') || str_starts_with($fileLines[1], '==');
        if (!$hasTitle) {
            throw new NoTitleFoundException(
                sprintf(
                    "No title where found in the two first lines of the files: \n%s\n%s",
                    $fileLines[0],
                    $fileLines[1],
                )
            );
        }

        $title = array_shift($fileLines) ?? '';
        if (str_starts_with($title, '# ')) {
            // Le titre de l'article est un H1 markdown en ligne (= commençant par `#`)
            $title = trim(substr($title, 1));
            array_shift($fileLines);
        } else {
            // Le titre de l'article est un H1 markdown souligné (= par une ligne de `=`)
            array_shift($fileLines);
            array_shift($fileLines);
        }

        return $title;
    }

    /**
     * Parse les métadonnées dans les lignes du fichier.
     *
     * Retourne ces métadonnées et les retire des lignes.
     *
     * @param string[]|array $fileLines
     *
     * @return mixed[]|array
     */
    private function parseMetadata(array &$fileLines): array
    {
        $metadata = [];
        foreach ($fileLines as $fileLine) {
            if (str_starts_with($fileLine, '[:') === false) {
                break;
            }

            $metaParts = [];
            // Regex de métadonnée markdown (ie. `[:name_of_metadata]: # "Its value"`)
            preg_match('@\[:([^]]+)]:\s#\s"([^"]+)"@', $fileLine, $metaParts);
            [, $metaName, $metaValue] = $metaParts;

            switch ($metaName) {
                case 'type':
                case 'version':
                    $metadata[$metaName] = $metaValue;
                    break;
                case 'sources':
                    $metadata[$metaName] = $this->parseSources($metaValue);
                    break;
                case 'created_at':
                case 'modified_at':
                    $metadata[$metaName] = \DateTime::createFromFormat(
                        'Y/m/d',
                        $metaValue
                    );
                    break;
                case 'tags':
                    $metadata[$metaName] = explode(' ', $metaValue);
                    break;
            }
            array_shift($fileLines);
        }

        if (\count($metadata) > 0) {
            array_shift($fileLines);
        }

        return [
            'type' => $metadata['type'] ?? null,
            'version' => $metadata['version'] ?? null,
            'sources' => $metadata['sources'] ?? [],
            'created_at' => $metadata['created_at'] ?? null,
            'modified_at' => $metadata['modified_at'] ?? null,
            'tags' => $metadata['tags'] ?? [],
        ];
    }

    /**
     * Parse les sources en argument et en retourne les liens.
     *
     * @param string $sourceLine
     *
     * @return array|Link[]
     */
    private function parseSources(string $sourceLine): array
    {
        $sources = explode(' | ', $sourceLine);

        $sourceLinks = [];
        foreach ($sources as $source) {
            $linkParts = [];
            // Regex de lien markdown (ie. `[Libellé](http://link.to)`)
            $isMatching = preg_match('@\[([^]]*)][(]([^)]+)[)]@', $source, $linkParts);
            if ($isMatching) {
                [, $label, $url] = $linkParts;
            } else {
                // Regex de lien markdown court (ie. `<http://link.to>`)
                $isMatching = preg_match('@<([^>]+)>@', $source, $linkParts);
                if (!$isMatching) {
                    continue;
                }

                [, $url] = $linkParts;
            }

            $sourceLinks[] = new Link($url, $label ?? $url);
        }

        return $sourceLinks;
    }

    /**
     * Modifie les URL internes (articles, assets, ...) pour qu'elles soient gérées par l'application.
     *
     * Exemple :
     *  - ./mon-article-image-01.png => /articles/tag-principal/mon-article-image-01.png
     *  - ./article-frère => /articles/tag-principal/article-frère
     *  - ./ailleurs/autre-article => /articles/ailleurs/autre-article
     *
     * @param string $content
     * @param string|null $mainTag
     *
     * @return string
     */
    private function modifyInternalUrl(string $content, ?string $mainTag): string
    {
        if (!str_contains($content, '](./')) {
            return $content;
        }

        /** @var Request|null $currentRequest */
        $currentRequest = $this->requestStack->getCurrentRequest();
        $baseUrl = $currentRequest !== null ? $currentRequest->getBasePath() : '';
        $maintTagUrlPart = $mainTag !== null ? $mainTag . '/' : '';

        // Regex de lien/image markdown (ie. `[Libellé](./link.to)`)
        // 3 groupes de capture :
        //  - [Libellé](
        //  - link.to
        //  - )
        $modifiedContent = preg_replace(
            [
                // Regexp différente pour les images, pour modifier différemment l'URL
                // (ajout du tag principal de l'article)
                '@(\!\[[^]]*]\()\./([^)]+)(\))@',
                '@(\[[^]]*]\()\./([^)]+)(\))@',
            ],
            [
                sprintf('$1%s/%s/%s$2$3', $baseUrl, $this->articleAssetPath, $maintTagUrlPart),
                sprintf('$1%s/articles/$2$3', $baseUrl),
            ],
            $content
        );

        return $modifiedContent ?? $content;
    }
}
