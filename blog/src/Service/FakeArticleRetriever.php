<?php

namespace App\Service;

use App\Entity\Article;

class FakeArticleRetriever implements ArticleRetrieverInterface
{

    /**
     * @inheritDoc
     */
    final public function retrieveArticle(string $mainTag, string $slug): Article
    {
        return (new Article())
            ->setTitle('Fake article')
            ->setType('Astuce')
            ->setSlug('fake-article')
            ->setContent(
                <<<EOF
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam non ante viverra, porttitor neque nec, faucibus nunc. Vestibulum congue efficitur est nec suscipit. Nullam vel dui est. In eu condimentum urna. Praesent ipsum elit, rhoncus nec venenatis eget, auctor eu arcu. Duis a pharetra sapien, at pretium risus. Suspendisse diam nisi, vulputate vel sem sollicitudin, blandit porta lorem. Maecenas sit amet sem iaculis, tincidunt lorem at, aliquam leo. Sed et nunc placerat, accumsan velit sit amet, dapibus velit. Nullam sed massa id massa scelerisque aliquet eu eu mi. Ut feugiat, justo eget finibus maximus, ipsum risus posuere augue, sed mattis quam justo mattis leo. Cras nisl mi, lacinia ut posuere vitae, fermentum a sapien. Nullam nisl ex, suscipit non lobortis quis, blandit quis libero. Morbi vitae consectetur dui.

Etiam elementum turpis eget augue elementum porta. Maecenas enim lectus, bibendum vel odio ac, elementum mollis est. Vestibulum eget elit maximus, semper libero ac, volutpat felis. Fusce eu vehicula odio, et aliquam lacus. Curabitur dictum rutrum turpis in cursus. Cras congue augue ac odio sollicitudin elementum. Suspendisse in tortor sed justo porta tempor. Ut viverra nulla sed vestibulum porta. Praesent non dui sed elit pulvinar volutpat sed sit amet nulla. Maecenas eu faucibus lorem.

Fusce viverra dolor vel pellentesque dapibus. Vestibulum malesuada leo id turpis suscipit tristique. Cras auctor vel purus id lacinia. Curabitur vel ornare ipsum. Nulla ligula nulla, gravida eget elementum ac, commodo eu nulla. Sed posuere suscipit sapien eu tristique. Sed mattis justo hendrerit aliquet vehicula. Vivamus ornare, mi a cursus mattis, lectus dui rutrum odio, ac scelerisque nunc tortor in massa.
EOF
            )
            ->setCreatedAt(new \DateTime())
            ->setModifiedAt(new \DateTime())
            ->setTags(['lorem', 'lipsum']);
    }

    /**
     * @inheritDoc
     */
    final public function retrieveAllArticles(): array
    {
        return [
            $this->retrieveArticle('lipsum', 'foo'),
            (new Article())
                ->setTitle('Another fake article')
                ->setType('Astuce')
                ->setSlug('another-fake-article')
                ->setContent(
                    <<<EOF
Nullam sit amet leo elit. Fusce ac risus convallis, dictum est at, gravida justo. Ut tincidunt ultrices felis sit amet ullamcorper. Ut porta tortor urna, quis lacinia nisl venenatis ut. Vestibulum elementum sit amet tortor sit amet ultricies. Etiam elementum sem dui, quis consequat lectus volutpat ac. Sed gravida blandit dictum. Vestibulum sed blandit nibh. Vivamus neque felis, cursus et nibh a, condimentum sollicitudin enim. Donec dignissim consequat pulvinar. Sed sit amet nisl non odio ornare auctor ut vitae velit. Mauris eu aliquet metus. Morbi tortor ligula, rutrum in imperdiet a, mollis id arcu. Vestibulum tempor purus eros, non luctus lacus pretium eu.

Morbi efficitur risus euismod justo tristique pharetra nec vel metus. Vestibulum at fermentum mauris. Nulla ultricies ligula non pharetra scelerisque. Nam bibendum neque sed turpis fermentum congue. Sed magna neque, finibus sed nibh vitae, sollicitudin dictum enim. Nam elit risus, egestas eu massa vel, imperdiet maximus nisi. Curabitur bibendum tellus dolor, vitae dictum diam tempor id. Duis nibh nisi, sodales at aliquam vel, ullamcorper id ipsum. Quisque iaculis, arcu vel tincidunt volutpat, quam nisi porttitor mi, et venenatis tortor sapien id mi. Suspendisse magna ligula, suscipit tincidunt consequat ut, laoreet eget erat. Nunc in imperdiet lorem. Fusce odio massa, hendrerit porta dui ut, laoreet egestas mauris. Donec lobortis convallis sem quis accumsan. In porttitor accumsan egestas. Suspendisse erat ligula, pulvinar eget magna ac, facilisis facilisis nunc. Cras elementum aliquam purus eget tempor.
EOF
                )
                ->setCreatedAt(new \DateTime())
                ->setModifiedAt(new \DateTime())
                ->setTags(['lipsum']),
            (new Article())
                ->setTitle('Last fake article')
                ->setType('Error')
                ->setSlug('last-fake-article')
                ->setContent(
                    <<<EOF
Nullam sit amet leo elit. Fusce ac risus convallis, dictum est at, gravida justo. Ut tincidunt ultrices felis sit amet ullamcorper. Ut porta tortor urna, quis lacinia nisl venenatis ut. Vestibulum elementum sit amet tortor sit amet ultricies. Etiam elementum sem dui, quis consequat lectus volutpat ac. Sed gravida blandit dictum. Vestibulum sed blandit nibh. Vivamus neque felis, cursus et nibh a, condimentum sollicitudin enim. Donec dignissim consequat pulvinar. Sed sit amet nisl non odio ornare auctor ut vitae velit. Mauris eu aliquet metus. Morbi tortor ligula, rutrum in imperdiet a, mollis id arcu. Vestibulum tempor purus eros, non luctus lacus pretium eu.

Morbi efficitur risus euismod justo tristique pharetra nec vel metus. Vestibulum at fermentum mauris. Nulla ultricies ligula non pharetra scelerisque. Nam bibendum neque sed turpis fermentum congue. Sed magna neque, finibus sed nibh vitae, sollicitudin dictum enim. Nam elit risus, egestas eu massa vel, imperdiet maximus nisi. Curabitur bibendum tellus dolor, vitae dictum diam tempor id. Duis nibh nisi, sodales at aliquam vel, ullamcorper id ipsum. Quisque iaculis, arcu vel tincidunt volutpat, quam nisi porttitor mi, et venenatis tortor sapien id mi. Suspendisse magna ligula, suscipit tincidunt consequat ut, laoreet eget erat. Nunc in imperdiet lorem. Fusce odio massa, hendrerit porta dui ut, laoreet egestas mauris. Donec lobortis convallis sem quis accumsan. In porttitor accumsan egestas. Suspendisse erat ligula, pulvinar eget magna ac, facilisis facilisis nunc. Cras elementum aliquam purus eget tempor.
EOF
                )
                ->setCreatedAt(new \DateTime())
                ->setModifiedAt(new \DateTime())
                ->setTags(['lorem']),
        ];
    }

    /**
     * @inheritDoc
     */
    final public function retrieveArticlesByTag(string $tag): array
    {
        return array_filter(
            $this->retrieveAllArticles(),
            static function ($article) use ($tag) {
                return \in_array($tag, $article->getTags());
            }
        );
    }

    /**
     * @inheritDoc
     */
    final public function retrieveArticlesByTerm(string $term): array
    {
        return array_filter(
            $this->retrieveAllArticles(),
            static function ($article) use ($term) {
                return \strpos($term, $article->getContent() ?? '') !== false;
            }
        );
    }
}
