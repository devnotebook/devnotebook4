<?php

namespace App\Service;

use App\Entity\Article;
use Symfony\Component\Finder\SplFileInfo;

interface ArticleBuilderInterface
{
    /**
     * Construit un objet à partir du fichier en argument.
     *
     * @param SplFileInfo $file
     *
     * @return Article
     */
    public function buildArticle(SplFileInfo $file): Article;
}
