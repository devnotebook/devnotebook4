<?php

namespace App\Service;

use App\Exception\ArticleWithNoTagException;
use App\Exception\EmptyArticleFileException;
use App\Exception\NoTitleFoundException;
use Psr\Log\LoggerInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;

class SlugifyArticleFileNamesService
{
    private Filesystem $fileSystem;
    private ArticleBuilderFromMarkdownFile $articleBuilder;
    private LoggerInterface $logger;
    private string $articleDirNamePath;

    public function __construct(
        Filesystem $fileSystem,
        ArticleBuilderFromMarkdownFile $articleBuilder,
        LoggerInterface $logger,
        string $articleDirNamePath
    ) {
        $this->fileSystem = $fileSystem;
        $this->articleBuilder = $articleBuilder;
        $this->logger = $logger;
        $this->articleDirNamePath = $articleDirNamePath;
    }

    /**
     * Cherche tous les fichiers d'article (contenu + images) et les renomme d'après le titre de l'article.
     *
     * @return \SplFileInfo[]|array
     */
    public function findAndSlugify(): array
    {
        $articleFiles = $this->findArticleFiles();

        return $this->renameArticleFiles($articleFiles);
    }


    /**
     * Retourne tous les fichiers contenants des articles ou leurs images.
     *
     * @return Finder
     */
    private function findArticleFiles(): Finder
    {
        return (new Finder())
            ->files()
            ->in($this->articleDirNamePath)
            ->name(['*.md', '*.png', '*.jpg']);
    }

    /**
     * Renomme tous les fichiers article à partir du titre de l'article.
     *
     * Mémorise les renommage effectués sur les articles pour pouvoir les reproduire sur les images.
     *
     * @param Finder $articleFiles
     *
     * @return \SplFileInfo[]|array
     */
    private function renameArticleFiles(Finder $articleFiles): array
    {
        $renamedFiles = [];
        $renamingMap = [];
        $imageFiles = [];

        $articleContentFileExtension = 'md';

        /** @var SplFileInfo $file */
        foreach ($articleFiles as $file) {
            if ($file->getExtension() !== $articleContentFileExtension) {
                $imageFiles[] = $file;
                continue;
            }

            try {
                $article = $this->articleBuilder->buildArticle($file);
            } catch (EmptyArticleFileException | NoTitleFoundException | ArticleWithNoTagException $e) {
                $this->logger->error($e->getMessage(), ['exception' => $e]);
                continue;
            }

            $currentFilePathName = $file->getPath() . '/' . $file->getFilenameWithoutExtension();
            $newFilePathName = sprintf(
                '%s/%s',
                $file->getPath(),
                $article->getSlug()
            );
            $renamingMap[$currentFilePathName] = $newFilePathName;

            $this->rename(
                $currentFilePathName,
                $newFilePathName,
                $articleContentFileExtension,
            );
            $renamedFiles[] = new \SplFileInfo($newFilePathName . '.' . $articleContentFileExtension);
        }

        foreach ($imageFiles as $imageFile) {
            $fileExtension = $imageFile->getExtension();
            $currentFilePathName = $imageFile->getPath() . '/' . $imageFile->getFilenameWithoutExtension();
            $imageNumber = substr($currentFilePathName, -3);
            $currentFilePathNameWithoutSuffix = substr($currentFilePathName, 0, -3);

            if (!\array_key_exists($currentFilePathNameWithoutSuffix, $renamingMap)) {
                $this->logger->error(
                    sprintf(
                        'Image file "%s" is not related on any article',
                        $currentFilePathName . '.' . $fileExtension,
                    )
                );
                continue;
            }

            $this->rename(
                $currentFilePathNameWithoutSuffix,
                $renamingMap[$currentFilePathNameWithoutSuffix],
                $fileExtension,
                $imageNumber
            );
            $renamedFiles[] = new \SplFileInfo($renamingMap[$currentFilePathNameWithoutSuffix] . '.' . $fileExtension);
        }

        return $renamedFiles;
    }

    /**
     * Renomme le fichier selon les arguments.
     *
     * @param string $currentFilePathName
     * @param string $newFilePathName
     * @param string $extension
     * @param string $suffix
     */
    private function rename(
        string $currentFilePathName,
        string $newFilePathName,
        string $extension,
        string $suffix = ''
    ): void {
        $this->logger->debug(
            sprintf(
                '"%s" => "%s"',
                $currentFilePathName,
                $newFilePathName,
            )
        );

        if ($currentFilePathName === $newFilePathName) {
            $this->logger->info(
                sprintf(
                    'File "%s" already slugified',
                    $currentFilePathName . $suffix . '.' . $extension,
                )
            );

            return;
        }

        $this->fileSystem->rename(
            $currentFilePathName . $suffix . '.' . $extension,
            $newFilePathName . $suffix . '.' . $extension
        );

        $this->logger->notice(
            sprintf(
                'File "%s" renamed to "%s"',
                $currentFilePathName . $suffix . '.' . $extension,
                $newFilePathName . $suffix . '.' . $extension,
            )
        );
    }
}
