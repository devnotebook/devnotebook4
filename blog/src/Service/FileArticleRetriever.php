<?php

namespace App\Service;

use App\Entity\Article;
use App\Exception\ArticleNotFoundException;
use Psr\Cache\InvalidArgumentException;
use Psr\Log\LoggerInterface;
use Symfony\Component\Cache\Adapter\AdapterInterface as CacheManager;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;

class FileArticleRetriever implements ArticleRetrieverInterface
{
    private Filesystem $fileSystem;
    private CacheManager $articleCacheManager;
    private ArticleBuilderInterface $articleBuilder;
    private LoggerInterface $logger;
    private string $articleDirNamePath;
    private string $articleAssetDirNamePath;

    public function __construct(
        Filesystem $fileSystem,
        CacheManager $articleCacheManager,
        ArticleBuilderInterface $articleBuilder,
        LoggerInterface $logger,
        string $articleDirNamePath,
        string $articleAssetDirNamePath
    ) {
        $this->fileSystem = $fileSystem;
        $this->articleCacheManager = $articleCacheManager;
        $this->articleBuilder = $articleBuilder;
        $this->logger = $logger;
        $this->articleDirNamePath = $articleDirNamePath;
        $this->articleAssetDirNamePath = $articleAssetDirNamePath;
    }

    /**
     * @inheritDoc
     */
    final public function retrieveArticle(string $mainTag, string $slug): Article
    {
        $finder = $this->getFinder()
            ->path($mainTag)
            ->name(sprintf('%s.md', $slug));

        if (!$finder->hasResults()) {
            throw new ArticleNotFoundException(
                sprintf(
                    'Article "%s" with tag "%s" not found',
                    $slug,
                    $mainTag
                )
            );
        }

        /** @var SplFileInfo $file */
        $file = current(iterator_to_array($finder));

        return $this->getArticleFromFile($file);
    }

    /**
     * @inheritDoc
     */
    final public function retrieveAllArticles(): array
    {
        $finder = $this->getFinder()
            ->name('*.md');

        $item = $this->articleCacheManager->getItem(
            sprintf('articles|all|%d', $finder->count())
        );

        if (!$item->isHit()) {
            $articles = $this->getArticles($finder);

            $item->set($articles);
            $this->articleCacheManager->save($item);
        }

        return $item->get();
    }

    /**
     * @inheritDoc
     */
    final public function retrieveArticlesByTag(string $tag): array
    {
        return array_filter(
            $this->retrieveAllArticles(),
            static function ($article) use ($tag) {
                return \in_array($tag, $article->getTags());
            }
        );
    }

    /**
     * @inheritDoc
     */
    final public function retrieveArticlesByTerm(string $term): array
    {
        return array_filter(
            $this->retrieveAllArticles(),
            static function ($article) use ($term) {
                return \stripos($article->getContent() ?? '', $term) !== false;
            }
        );
    }

    /**
     * Retourne les articles présents dans les fichiers en arguments.
     *
     * @param Finder $finder
     *
     * @return Article[]|array
     */
    private function getArticles(Finder $finder): array
    {
        $articles = [];
        foreach ($finder as $file) {
            try {
                $articles[] = $this->getArticleFromFile($file);
            } catch (\Exception$e) {
                $this->logger->error(
                    sprintf(
                        'Cannot extract article from file "%s" : %s',
                        $file->getPathname(),
                        $e->getMessage()
                    )
                );
            }
        }

        usort(
            $articles,
            static function (Article $a, Article $b) {
                return -($a->getCreatedAt() <=> $b->getCreatedAt());
            }
        );

        return $articles;
    }

    /**
     * Retourne le contenu du fichier sous forme d'article.
     *
     * Utilise l'article en cache si possible.
     *
     * @param SplFileInfo $file
     *
     * @return Article
     */
    private function getArticleFromFile(SplFileInfo $file): Article
    {
        $cacheKey = 'articles|unique|' . md5($file->getContents());

        try {
            $item = $this->articleCacheManager->getItem($cacheKey);
            if (!$item->isHit()) {
                $item->set($this->articleBuilder->buildArticle($file));
                $this->prepareArticleAssets($file);
                $this->articleCacheManager->save($item);
            }

            $article = $item->get();
        } catch (InvalidArgumentException $e) {
            $this->logger->error(
                sprintf(
                    'Cannot search cache for article file "%s", with key "%s',
                    $file->getPathname(),
                    $cacheKey
                ),
                ['exception' => $e]
            );

            $article = $this->articleBuilder->buildArticle($file);
        }

        return $article;
    }

    /**
     * Retourne une instance de Finder initialisé pour rechercher dans le répertoire racine des articles.
     *
     * @return Finder
     */
    private function getFinder(): Finder
    {
        return (new Finder())
            ->files()
            ->in($this->articleDirNamePath);
    }

    /**
     * Prépare les assets associés à l'article.
     *
     * @param SplFileInfo $articleFile
     */
    private function prepareArticleAssets(SplFileInfo $articleFile): void
    {
        $articlePathParts = explode('/', $articleFile->getRelativePath());
        $tag = array_pop($articlePathParts);

        $assets = $this->getArticleAssets($articleFile, $tag);
        $this->copyAssets($assets, $tag);
    }

    /**
     * Crée des copies des assets associés à l'article dans le répertoire public de l'application.
     *
     * @param SplFileInfo[]|iterable $assets
     * @param string $tag
     */
    private function copyAssets(iterable $assets, string $tag): void
    {
        foreach ($assets as $asset) {
            $currentFile = $asset->getPathname();
            $targetFile = sprintf(
                '%s/%s/%s',
                $this->articleAssetDirNamePath,
                $tag,
                $asset->getFilename()
            );

            $this->fileSystem->copy($currentFile, $targetFile, true);
        }
    }

    /**
     * Retourne la liste des assets associés à l'article.
     *
     * @param SplFileInfo $articleFile
     *
     * @param string $tag
     *
     * @return Finder
     */
    private function getArticleAssets(SplFileInfo $articleFile, string $tag): Finder
    {
        $imageFileRegex = sprintf(
            '@^%s-\d{2}\.(png|jpg)$@',
            preg_quote($articleFile->getFilenameWithoutExtension(), '@')
        );

        return $this->getFinder()
            ->name($imageFileRegex)
            ->path($tag);
    }
}
