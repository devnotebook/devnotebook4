<?php

namespace App\Entity;

class Article
{
    /** @var string|null */
    private ?string $title = null;

    /** @var string|null */
    private ?string $type = null;

    /** @var string|null */
    private ?string $slug = null;

    /** @var string|null */
    private ?string $content = null;

    /** @var array|Link[] */
    private array $sources = [];

    /** @var string|null */
    private ?string $version = null;

    /** @var string[]|array */
    private array $tags = [];

    /** @var \DateTime|null */
    private ?\DateTime $createdAt = null;

    /** @var \DateTime|null */
    private ?\DateTime $modifiedAt = null;

    /**
     * Retourne le tag principal.
     *
     * @return string|null
     */
    public function getMainTag(): ?string
    {
        return $this->getTags()[0] ?? null;
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string|null $title
     *
     * @return Article
     */
    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string|null $type
     *
     * @return Article
     */
    public function setType(?string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSlug(): ?string
    {
        return $this->slug;
    }

    /**
     * @param string|null $slug
     *
     * @return Article
     */
    public function setSlug(?string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getContent(): ?string
    {
        return $this->content;
    }

    /**
     * @param string|null $content
     *
     * @return Article
     */
    public function setContent(?string $content): self
    {
        $this->content = $content;

        return $this;
    }

    /**
     * @return array|Link[]
     */
    public function getSources(): array
    {
        return $this->sources;
    }

    /**
     * @param array|Link[] $sources
     *
     * @return Article
     */
    public function setSources(array $sources): self
    {
        $this->sources = $sources;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getVersion(): ?string
    {
        return $this->version;
    }

    /**
     * @param string|null $version
     *
     * @return Article
     */
    public function setVersion(?string $version): self
    {
        $this->version = $version;

        return $this;
    }

    /**
     * @return array|string[]
     */
    public function getTags(): array
    {
        return $this->tags;
    }

    /**
     * @param array|string[] $tags
     *
     * @return Article
     */
    public function setTags(array $tags): self
    {
        $this->tags = $tags;

        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime|null $createdAt
     *
     * @return Article
     */
    public function setCreatedAt(?\DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getModifiedAt(): ?\DateTime
    {
        return $this->modifiedAt;
    }

    /**
     * @param \DateTime|null $modifiedAt
     *
     * @return Article
     */
    public function setModifiedAt(?\DateTime $modifiedAt): self
    {
        $this->modifiedAt = $modifiedAt;

        return $this;
    }
}
