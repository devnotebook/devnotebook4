<?php

namespace App\Entity;

class CloudTag
{
    private string $name;
    private float $weight;
    private int $nbOfArticles;

    public function __construct(string $name, float $weight = 0, int $nbOfArticles = 0)
    {
        $this->name = $name;
        $this->weight = $weight;
        $this->nbOfArticles = $nbOfArticles;
    }

    /**
     * Incrémente le nombre d'articles.
     */
    public function incNbOfArticles(): self
    {
        $this->nbOfArticles++;

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return CloudTag
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return float
     */
    public function getWeight(): float
    {
        return $this->weight;
    }

    /**
     * @param float $weight
     *
     * @return CloudTag
     */
    public function setWeight(float $weight): self
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * @return int
     */
    public function getNbOfArticles(): int
    {
        return $this->nbOfArticles;
    }

    /**
     * @param int $nbOfArticles
     *
     * @return CloudTag
     */
    public function setNbOfArticles(int $nbOfArticles): self
    {
        $this->nbOfArticles = $nbOfArticles;

        return $this;
    }
}
