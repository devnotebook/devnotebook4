<?php

namespace App\Entity;

class Link
{
    private ?string $label;
    private string $url;

    public function __construct(string $url, ?string $label = null)
    {
        $this->url = $url;
        $this->label = $label;
    }

    /**
     * @return string|null
     */
    public function getLabel(): ?string
    {
        return $this->label ?: $this->getUrl();
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }
}
