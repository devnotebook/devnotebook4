<?php

namespace App\Command;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\String\Slugger\SluggerInterface;

#[AsCommand('slugify')]
class SlugifyCommand extends Command
{
    private SluggerInterface $slugger;

    public function __construct(SluggerInterface $slugger)
    {
        $this->slugger = $slugger;
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->setDescription('Slugify the provided string')
            ->addArgument('string', InputArgument::REQUIRED, 'String to slugify');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        /** @var string $stringToSlugify */
        $stringToSlugify = $input->getArgument('string');
        $slug = strtolower(
            $this->slugger->slug($stringToSlugify)
        );

        $io->note(sprintf('String provided : "%s"', $stringToSlugify));
        $io->note(sprintf('Slug : "%s"', $slug));

        return Command::SUCCESS;
    }
}
