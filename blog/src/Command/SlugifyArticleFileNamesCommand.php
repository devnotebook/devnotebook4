<?php

namespace App\Command;

use App\Service\SlugifyArticleFileNamesService;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand('app:slugify-article-file-names')]
class SlugifyArticleFileNamesCommand extends Command
{
    private SlugifyArticleFileNamesService $slugifyArticleFileNamesService;

    public function __construct(
        SlugifyArticleFileNamesService $slugifyArticleFileNamesService
    ) {
        $this->slugifyArticleFileNamesService = $slugifyArticleFileNamesService;
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->setDescription('Slugify all article file names (markdown and images files');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $start = microtime(true);

        $slugifiedFiles = $this->slugifyArticleFileNamesService->findAndSlugify();

        $io = new SymfonyStyle($input, $output);

        $io->success(
            sprintf(
                '%d file(s) renamed (in %dms)',
                \count($slugifiedFiles),
                $start - microtime(true)
            )
        );

        return Command::SUCCESS;
    }
}
