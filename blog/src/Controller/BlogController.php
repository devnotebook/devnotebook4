<?php

namespace App\Controller;

use App\Service\ArticleRetrieverInterface;
use App\Service\TagCloudBuilderInterface;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Attribute\Cache;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;

class BlogController
{
    private ArticleRetrieverInterface $articleRetriever;
    private TagCloudBuilderInterface $tagCloudBuilder;
    private Environment $twigEnvironment;

    public function __construct(
        ArticleRetrieverInterface $articleRetriever,
        TagCloudBuilderInterface $tagCloudBuilder,
        Environment $twigEnvironment
    ) {
        $this->articleRetriever = $articleRetriever;
        $this->tagCloudBuilder = $tagCloudBuilder;
        $this->twigEnvironment = $twigEnvironment;
    }

    #[Route(path: '/articles/{mainTag<[a-zA-Z0-9-_]+>}/{slug<[a-zA-Z0-9-_]+>}', name: 'show_article', methods: ['GET'], priority: 1)]
    #[Cache(maxage: 3600)]
    public function showArticle(string $mainTag, string $slug, Request $request): Response
    {
        $article = $this->articleRetriever->retrieveArticle($mainTag, $slug);

        $nbArticlesPerPage = $this->getNbArticlesPerPage($request);

        $response = new Response(
            $this->twigEnvironment->render(
                'blog/show_article.html.twig',
                [
                    'article' => $article,
                    'nbArticlesPerPage' => $this->getNbArticlesPerPage($request),
                    'tagCloud' => $this->tagCloudBuilder->build(),
                ]
            )
        );

        $this->setNbArticlesPerPageTooCookies($response, $nbArticlesPerPage);

        return $response;
    }

    #[Route(path: '/articles', name: 'show_all_articles', methods: ['GET'])]
    #[Cache(maxage: 120)]
    public function showAllArticles(Request $request): Response
    {
        $articles = $this->articleRetriever->retrieveAllArticles();

        $nbArticlesPerPage = $this->getNbArticlesPerPage($request);

        $response = new Response(
            $this->twigEnvironment->render(
                'blog/show_all_articles.html.twig',
                [
                    'articles' => \array_slice($articles, 0, $nbArticlesPerPage),
                    'nbArticlesPerPage' => $nbArticlesPerPage,
                    'nbArticles' => \count($articles),
                    'tagCloud' => $this->tagCloudBuilder->build(),
                ]
            )
        );

        $this->setNbArticlesPerPageTooCookies($response, $nbArticlesPerPage);

        return $response;
    }

    #[Route(path: '/articles/{tag<[a-zA-Z0-9-]+>}', name: 'show_article_by_tag', methods: ['GET'])]
    #[Cache(maxage: 120)]
    public function showArticlesByTag(string $tag, Request $request): Response
    {
        $articles = $this->articleRetriever->retrieveArticlesByTag($tag);

        if (\count($articles) === 0) {
            throw new NotFoundHttpException('Tag inconnu');
        }

        $viewMode = $request->query->get('view', 'full');
        $template = $viewMode === 'full'
            ? 'blog/show_articles_by_tag.html.twig'
            : 'blog/show_articles_by_tag_as_list.html.twig';

        $nbArticlesPerPage = $viewMode === 'full'
            ? $this->getNbArticlesPerPage($request)
            : 99999;

        $response = new Response(
            $this->twigEnvironment->render(
                $template,
                [
                    'articles' => \array_slice($articles, 0, $nbArticlesPerPage),
                    'nbArticles' => \count($articles),
                    'nbArticlesPerPage' => $nbArticlesPerPage,
                    'tag' => $tag,
                    'tagCloud' => $this->tagCloudBuilder->build(),
                ]
            )
        );

        $this->setNbArticlesPerPageTooCookies($response, $nbArticlesPerPage);

        return $response;
    }

    #[Route(path: '/recherche', name: 'search', methods: ['GET'])]
    #[Cache(maxage: 120)]
    public function search(Request $request): Response
    {
        $searchTerm = urldecode((string) $request->query->get('term'));
        $articles = $this->articleRetriever->retrieveArticlesByTerm($searchTerm);

        return new Response(
            $this->twigEnvironment->render(
                'blog/show_search_results.html.twig',
                [
                    'searchTerm' => $searchTerm,
                    'articles' => $articles,
                    'nbArticles' => \count($articles),
                    'nbArticlesPerPage' => $this->getNbArticlesPerPage($request),
                    'tagCloud' => $this->tagCloudBuilder->build(),
                ]
            )
        );
    }

    #[Route(path: '/feed', name: 'rss_all_articles', methods: ['GET'])]
    #[Cache(maxage: 120)]
    public function rssAllArticles(Request $request): Response
    {
        $articles = $this->articleRetriever->retrieveAllArticles();

        $nbArticlesPerPage = $this->getNbArticlesPerPage($request);

        $response = new Response(
            $this->twigEnvironment->render(
                'blog/rss_all_articles.xml.twig',
                [
                    'articles' => \array_slice($articles, 0, $nbArticlesPerPage),
                ]
            )
        );

        return $response;
    }

    private function getNbArticlesPerPage(Request $request): int
    {
        return (int) $request->query->getDigits('nbArticlesPerPage', '0')
            ?: (int) $request->cookies->get('nbArticlesPerPage', '10');
    }

    private function setNbArticlesPerPageTooCookies(Response $response, int $nbArticlesPerPage): void
    {
        $response->headers->setCookie(Cookie::create('nbArticlesPerPage', (string) $nbArticlesPerPage));
    }
}
