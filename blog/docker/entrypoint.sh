#!/bin/bash -x

if [ ! -f "/srv/app/.env.local" ]; then
    echo "Create .env.local to activate dev mode"
    printf "###> symfony/framework-bundle ###\nAPP_ENV=dev\n###< symfony/framework-bundle ###" > /srv/app/.env.local
fi

composer install

./symfony server:ca:install
./symfony serve
